function randomizeBoardState() {
  var maxIndex = PATTERNS.length;
  var tiles = document.getElementsByTagName('td');
  var patternElement = document.getElementById('patternCount');
  var patternCount = Math.min(patternElement.value, maxIndex - 1);
  
  // set board to pattern
  Actions.startRecord();
  for (var k = 0; k < parseInt(patternCount); k++) {
    var randomIndex = Math.floor(Math.random() * maxIndex);
    var pattern = PATTERNS[randomIndex];
    for (var i = 0; i < 15; i++) {
      for (var j = 0; j < 15; j++) {
        var val = pattern[i][j];
        if (val != 3) {
          var tile = tiles.item(j * 15 + i);
          Actions.addAction(tile, tile.getAttribute('bv'));
          updateTileBoardValue.call(tile, false, false, val);
        }
      }
    }
  }
  Actions.endRecord();
  Logger.log(`Patternized board #${patternCount}`);
}

function clearBoard() {
  var tiles = document.getElementsByTagName('td');
  Actions.startRecord();
  for (var tile of tiles) {
    Actions.addAction(tile, tile.getAttribute('bv'));
    updateTileBoardValue.call(tile, false, false, 3);
  }
  Actions.endRecord();
  Logger.log("Cleared board");
}

function voidInversion() {
  var tiles = document.getElementsByTagName('td');
  Actions.startRecord();
  for (var tile of tiles) {
    var bv = tile.getAttribute('bv');
    if (["0", "3"].includes(bv)) {
      var val = bv === "0" ? "3" : "0";
      Actions.addAction(tile, tile.getAttribute('bv'));
      updateTileBoardValue.call(tile, false, false, val);
    }
  }
  Actions.endRecord();
  Logger.log("Inverted board");
}