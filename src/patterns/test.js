function testPatterns() {
  // verify there are no duplicate patterns
  var mappedPatterns = PATTERNS.map(pattern => pattern.join(','));
  var setOfPatterns = new Set(mappedPatterns);
  console.log(mappedPatterns.length, setOfPatterns.size);
  if (mappedPatterns.length !== setOfPatterns.size) {
    throw new Error('There exist duplicate patterns within the data.');
  }
}