// when page loads, create the board and add listeners
function init() {
  runTests();
  // create board tiles
  var tableElement = document.getElementById('board');
  tableElement.innerHTML = '';
  for (var i = 0; i < 15; i++) {
    var row = '<tr>';
    for (var j = 0; j < 15; j++) {
      var isCenterTile = i == 7 && j == i;
      var grayscaleClass = (i + j) % 2 == 0 ? 'white' : 'dark';
      var tile = `<td class="${grayscaleClass} ${isCenterTile ? 'center' : ''}" bv="${isCenterTile ? 0 : 3}" xv="${i}" yv="${j}"></td>`;
      row += tile;
    }
    row += '</tr>';
    tableElement.innerHTML += row;
  }
  addTileListeners();
  addImportListeners();
  addBoardSelectors();
  outputBoard();

  window.addEventListener('keydown', addSaveLoadKeyListener);
}

/**
 * Adds listeners to board tiles.
 */
function addTileListeners() {
  var tiles = document.getElementsByTagName('td');
  for (var tile of tiles) {
    tile.addEventListener('mouseenter', Actions.startRecord);
    tile.addEventListener('mousedown', addActionListener);
    tile.addEventListener('mouseenter', updateTileBoardValueWhileClicked);
    tile.addEventListener('mousedown', updateTileBoardValue);
  }
}

function runTests() {
  try {
    testPatterns();
  } catch (e) {
    var element = document.getElementsByTagName('body')[0];
    element.innerHTML = e;
    throw new Error(e);
  }
}

window.addEventListener('load', init);
window.addEventListener('mouseup', Actions.endRecord);