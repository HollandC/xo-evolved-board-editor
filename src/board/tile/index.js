// when a tile is clicked, update its board value and set text
function updateTileBoardValue(useHsym = false, useVsym = false, setBV = -1) {
  var newValue = setBV === -1 ? getCurrentBoardValuePicker() : setBV;
  if (Actions.groupActionOccurring) {
    Actions.addAction(this, this.getAttribute('bv'));
  }
  this.setAttribute('bv', newValue);
  this.innerText = mapBoardValueToText(newValue);
  var xv = parseInt(this.getAttribute('xv'));
  var yv = parseInt(this.getAttribute('yv'));
  if (useHsym || useVsym) {
    var vsym = Symmetry.vertical;
    var hsym = Symmetry.horizontal;
    if (vsym || hsym) {
      var tiles = document.getElementsByTagName('td');
      for (var tile of tiles) {
        var ix = invertNumber(xv);
        var iy = invertNumber(yv);
        var tx = parseInt(tile.getAttribute('xv'));
        var ty = parseInt(tile.getAttribute('yv'));
        if (hsym) {
          if (tx == ix && ty == yv) {
            updateTileBoardValue.call(tile);
          }
        }
        if (vsym) {
          if (ty == iy && tx == xv) {
            updateTileBoardValue.call(tile);
          }
        }
        if (hsym && vsym) {
          if (tx == ix && ty == iy) {
            updateTileBoardValue.call(tile);
          }
        }
      }
    }
  }
  // Logger.log(`Updated tile (${yv},${xv}) to ${newValue} ${mapBoardValueToText(newValue)}`);
  outputBoard();
}

// same as above, but when lmb is held down
function updateTileBoardValueWhileClicked() {
  if (event.buttons != 1) {
    return;
  }
  Actions.addAction(this, this.getAttribute('bv'));
  updateTileBoardValue.call(this, true, true);
}