class Symmetry {
  static horizontal = false;
  static vertical = false;

  static toggleHorizontal() {
    Symmetry.horizontal = !Symmetry.horizontal;
    var btn = document.getElementById('vsym');
    btn.classList.forEach(cls => btn.classList.remove(cls));
    btn.classList.add(Symmetry.horizontal ? 'toggle-on' : 'toggle-off');
  }

  static toggleVertical() {
    Symmetry.vertical = !Symmetry.vertical;
    var btn = document.getElementById('hsym');
    btn.classList.forEach(cls => btn.classList.remove(cls));
    btn.classList.add(Symmetry.vertical ? 'toggle-on' : 'toggle-off');
  }
}