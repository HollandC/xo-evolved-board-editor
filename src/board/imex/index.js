/**
 * Copies the board state to clipboard.
 */
function copyToClipboard() {
  // Get the text field
  var copyElement = document.getElementById("copier");

  // Select the text field
  copyElement.select(); 

    // Copy the text inside the text field
  navigator.clipboard.writeText(copyElement.value);

  Logger.log('Copied board state');
}

/**
 * Imports board state using text input.
 */
function importBoardState() {
  var importInput = document.getElementById('importBoardInput');
  var board = importInput.value;
  if (board.length < 2) {
    importInput.value = "";
    Highlighter.setElements([this, importInput], HIGHLIGHT_RED);
    return;
  }
  var parse = [...board.matchAll(/\d+/g)];
  if (parse.length !== 225) {
    importInput.value = "";
    Highlighter.setElements([this, importInput], HIGHLIGHT_RED);
    return;
  }
  Highlighter.setElement(this);
  var formatted = [];
  var data = [];
  for (var dat of parse) {
    data.push(dat[0]);
    if (data.length === 15) {
      formatted.push(data);
      data = [];
    }
  }
  Actions.startRecord();
  var tiles = document.getElementsByTagName('td');
  for (var i = 0; i < 15; i++) {
    for (var j = 0; j < 15; j++) {
      var val = formatted[i][j];
      var tile = tiles.item(j * 15 + i);
      Actions.addAction(tile, tile.getAttribute('bv'));
      updateTileBoardValue.call(tile, false, false, val);
    }
  }
  Actions.endRecord();
  importInput.value = "";
  Logger.log('Imported board state');
}

function outputBoard() {
  var copier = document.getElementById('copier');
  var str = "[\n";
  var tiles = document.getElementsByTagName('td');
  for (var i = 0; i < 15; i++) {
    var data = [];
    for (var j = 0; j < 15; j++) {
      // yes...
      var tile = tiles.item(j * 15 + i);
      data.push(tile.getAttribute('bv'));
    }
    str += `[${data}]${i == 14 ? '' : ',\n'}`;
  }
  str += "],";
  copier.value = str;
}

function addImportListeners() {
  var importButton = document.getElementById('importButton');
  importButton.addEventListener('click', importBoardState);
}