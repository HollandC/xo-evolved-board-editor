class Actions {
  static history = [];
  static future = [];
  static groupActionOccurring = false;
  static undo() {
    if (Actions.history.length >= 1) {
      var lastAction = Actions.history[0];
      Actions.future.unshift(Actions.redoizeAction(lastAction));
      if (Array.isArray(lastAction)) {
        if (lastAction.length === 0) {
          Actions.history.shift();
          Actions.undo();
          return;
        } else {
          for (var action of lastAction) {
            var t = action.element;
            Logger.log(`Undo ${t.getAttribute('xv')},${t.getAttribute('yv')} to ${action.oldValue}`)
            updateTileBoardValue.call(t, false, false, action.oldValue);
          }
        }
      } else {
        var t = lastAction.element;
        Logger.log(`Undo ${t.getAttribute('xv')},${t.getAttribute('yv')} to ${lastAction.oldValue}`)
        updateTileBoardValue.call(t, false, false, lastAction.oldValue);
      }
      Actions.history.shift();
    }
    Actions.updateAppearance();
  }

  static redoizeAction(arrayOfActions) {
    var newAction = [];
    for (var action of arrayOfActions) {
      var tile = action.element;
      var val = tile.getAttribute('bv');
      newAction.push({
        element: tile,
        oldValue: val,
      });
    }
    return newAction;
  }

  static redo() {
    if (Actions.future.length >= 1) {
      Actions.startRecord();
      var futureAction = Actions.future[0];
      if (Array.isArray(futureAction)) {
        if (futureAction.length === 0) {
          Actions.future.shift();
          Actions.redo();
          return;
        } else {
          for (var action of futureAction) {
            var t = action.element;
            Logger.log(`Undo ${t.getAttribute('xv')},${t.getAttribute('yv')} to ${action.oldValue}`)
            updateTileBoardValue.call(t, false, false, action.oldValue);
          }
        }
      } else {
        var t = lastAction.element;
        Logger.log(`Undo ${t.getAttribute('xv')},${t.getAttribute('yv')} to ${lastAction.oldValue}`)
        updateTileBoardValue.call(t, false, false, lastAction.oldValue);
      }
      Actions.future.shift();
      Actions.endRecord();
    }
    Actions.updateAppearance();
  }

  static addAction(element, oldValue) {
    var data = {
      element: element,
      oldValue: oldValue,
    };
    Logger.log(`Adding action to log: ${oldValue}`);

    if (Actions.groupActionOccurring) {
      if (!Actions.history[0].some(act => act.element === element)) {
        Actions.history[0].push(data);
      }
    } else {
      // alert('aidsnfaoinsdga');
      // Actions.history.unshift(data);
    }
    Actions.updateAppearance();
  }

  static startRecord() {
    if (!Actions.groupActionOccurring) {
      Logger.log('Start record');
      Actions.history.unshift([]);
      Actions.groupActionOccurring = true;
    }
  }

  static endRecord() {
    Actions.groupActionOccurring = false;
    Logger.log('End record');
  }

  static updateAppearance() {
    var undoButton = document.getElementById('undo');
    var redoButton = document.getElementById('redo');
    if (Actions.history.some(action => action.length > 0)) {
      undoButton.classList.add('clickable');
    } else {
      undoButton.classList.remove('clickable');
    }
    if (Actions.future.some(action => action.length > 0)) {
      redoButton.classList.add('clickable');
    } else {
      redoButton.classList.remove('clickable');
    }
  }
}

function addActionListener() {
  Actions.addAction(this, this.getAttribute('bv'));
  Actions.future = [];
}