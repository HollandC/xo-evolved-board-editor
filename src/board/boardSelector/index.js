// create quick selects
function addBoardSelectors() {
  var ids = ['boardSelectorPalette'];
  for (var id of ids) {
    var selectorArea = document.getElementById(id);
    var vals = supportedBoardValues;
    for (var val of vals) {
      var id = `bs${val}`;
      var text = mapBoardValueToText(val);
      var boardSelector = `<button id="${id}" class="boardSelector" value="${val}">${text}</button>`;
      selectorArea.innerHTML += boardSelector;
    }
    var elements = document.getElementsByClassName('boardSelector');
    for (var element of elements) {
      if (element.value == "0") {
        element.classList.add('active');
      }
      element.addEventListener('click', updateBoardSelector);
    }
  }
}
  
// set current board value
function updateBoardSelector() {
  Logger.log(`Clicked board selector ${this.value} ${mapBoardValueToText(this.value)}`)
  resetBoardSelectorClass();
  this.classList.add('active');
  var val = this.value;
  updateBoardValueInput(val);
  var tab = document.getElementById('boardSelectorTab');
  tab.setAttribute('sel', val);
}

function resetBoardSelectorClass() {
  var boardSelectors = document.getElementsByClassName('boardSelector');
  for (var bs of boardSelectors) {
    bs.classList.remove('active');
  }
}

function getCurrentBoardValuePicker() {
  var inputElement = document.getElementById('boardValueInput');
  var newValue = inputElement.value;
  return parseInt(newValue);
}

function updateBoardValueInput(val) {
  var inputElement = document.getElementById('boardValueInput');
  inputElement.value = val;
}