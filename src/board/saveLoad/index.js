class SaveLoad {
  static boardState = [];
  static actionHistory = [];
  static actionFuture = [];
  static save() {
    var loadButton = document.getElementById('loadBoardState');
    if (!loadButton.classList.contains('clickable')) {
      loadButton.classList.add('clickable');
    }
    Logger.log('save board state');
    var tiles = document.getElementsByTagName('td');
    SaveLoad.boardState = [];
    var data = [];
    for (var tile of tiles) {
      data.push(tile.getAttribute('bv'));
      if (data.length === 15) {
        SaveLoad.boardState.push(data);
        data = [];
      }
    }
    SaveLoad.actionHistory = [...Actions.history];
    SaveLoad.actionFuture = [...Actions.future];
  }

  static load() {
    if (SaveLoad.boardState.length === 15) {
      Logger.log('Loaded board state');
      var tiles = document.getElementsByTagName('td');
      for (var i = 0; i < 15; i++) {
        for (var j = 0; j < 15; j++) {
          var bv = SaveLoad.boardState[i][j];
          updateTileBoardValue.call(tiles[i * 15 + j], false, false, bv);
        }
      }
      Actions.history = SaveLoad.actionHistory;
      Actions.future = SaveLoad.actionFuture;
    }
  }
}

function addSaveLoadKeyListener() {
  switch (event.key) {
    case "s":
      SaveLoad.save();
      break;
    case "l":
      SaveLoad.load();
      break;
  }
}