/**
 * Inverts number relative to board (15x15 board).
 * 0 -> 14
 * 1 -> 13
 * 14 -> 0
 * @param {number} coord coord
 * @returns {number} inverted coord
 */
function invertNumber(coord) {
  if (coord < 0) {
    coord = 0;
  }
  if (coord > 14) {
    coord = 14;
  }
  return Math.abs(coord - 14);
}