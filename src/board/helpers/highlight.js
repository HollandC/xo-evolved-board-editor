const HIGHLIGHT_GREEN = 'hl-green';
const HIGHLIGHT_RED = 'hl-red';

class Highlighter {
  static elements = [];
  static interval = undefined;
  static highlightStyle = undefined;

  static setElement(element, hlStyle = HIGHLIGHT_GREEN) {
    Highlighter.removeHighlight();
    Highlighter.elements = [element];
    clearInterval(Highlighter.interval);
    Highlighter.highlightStyle = hlStyle;
    Highlighter.addHighlight();
    Highlighter.interval = setInterval(Highlighter.removeHighlight, 1000);
  }

  static setElements(elements, hlStyle = HIGHLIGHT_GREEN) {
    Highlighter.removeHighlight();
    Highlighter.elements = elements;
    clearInterval(Highlighter.interval);
    Highlighter.highlightStyle = hlStyle;
    Highlighter.addHighlight();
    Highlighter.interval = setInterval(Highlighter.removeHighlight, 1000);
  }

  static addHighlight() {
    for (var element of Highlighter.elements) {
      element.classList.add(Highlighter.highlightStyle);
    }
  }
  
  static removeHighlight() {
    if (Highlighter.elements.length > 0) {
      for (var element of Highlighter.elements) {
        element.classList.remove(Highlighter.highlightStyle);
        clearInterval(Highlighter.interval);
      }
      Highlighter.elements = [];
    }
  }
}