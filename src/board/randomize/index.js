function pureRandomizeBoardState() {
  var tiles = document.getElementsByTagName('td');
  var maxIndex = weightedBoardValues.length;
  
  for (var i = 0; i < 15; i++) {
    for (var j = 0; j < 15; j++) {
      var randomIndex = Math.floor(Math.random() * maxIndex);
      var val = weightedBoardValues[randomIndex];
      var tile = tiles.item(j * 15 + i);
      updateTileBoardValue.call(tile, false, false, val);
    }
  }
  Logger.log('Randomized board (PU)');
}

function basicRandomizeBoardState() {
  var tiles = document.getElementsByTagName('td');
  var weightElement = document.getElementById('basicWeightInput');
  var weight = parseFloat(weightElement.value);
  while (weight > 1) {
    weight /= 10;
    weightElement.value = weight;
  }
  
  for (var i = 0; i < 15; i++) {
    for (var j = 0; j < 15; j++) {
      var rng = Math.random();
      var val = rng < weight ? 0 : 3;
      var tile = tiles.item(j * 15 + i);
      updateTileBoardValue.call(tile, false, false, val);
    }
  }
  Logger.log('Randomized board (W)');
}