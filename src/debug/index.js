class Logger {
  static LOG_LENGTH = 4;
  static log(desc) {
    if (DEBUG_MODE) {
      var logElement = document.getElementById('logger');
      var newElement = `<span class="log">${desc}<br></span>`;
      logElement.innerHTML = newElement + logElement.innerHTML;
      Logger.cleanupLogs();
    }
  }

  static getLogs() {
    return DEBUG_MODE ? document.getElementsByClassName("log") : [];
  }
  
  static cleanupLogs() {
    if (DEBUG_MODE) {
      var logs = Logger.getLogs();
      if (logs.length > Logger.LOG_LENGTH) {
        var last = logs[logs.length - 1];
        last.remove();
      }
    }
  }
}