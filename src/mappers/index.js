function mapBoardValueToText(value) {
  value = parseInt(value);
  var txt = '';
  switch (value) {
    case 0:
      txt = ' ';
      break;
    case 1:
      txt = 'X';
      break;
    case 2:
      txt = 'O';
      break;
    case 4:
      txt = '/';
      break;
    case 5:
      txt = '_';
      break;
    case 6:
      txt = '|';
      break;
    case 9:
      txt = '\\';
      break;
    case 11:
      txt = 'W';
      break;
    case 13:
      txt = 'G';
      break;
    case 16:
      txt = 'A';
      break;
    case 21:
      txt = 'S';
      break;
  }
  return txt;
}